﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BonusApp
{
    public delegate double BonusProvider(double amount);

    //public Func<double, double> BonusProvider;

    public class Bonuses
    {
        public static Func<double, double> TenPercent = (input) => { return input / 10.0; };

        //private static double tenPercent(double amount)
        //{
        //    return amount / 10.0;
        //}

        public static Func<double, double> FlatTwoIfAmountMoreThanFive = (input) => { return input > 5.0 ? 2.0 : 0.0; };

        //private static double flatTwoIfAmountMoreThanFive(double amount)
        //{
        //    return amount > 5.0 ? 2.0 : 0.0;
        //}
    }
}
